﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace YzApi.Controllers
{
    /// <summary>
    /// 设置了默认的route convention，
    /// 要求DbContext的依赖注入
    /// </summary>
    [Route("[controller]/[action]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private DbContext _dbContext;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public BaseController(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbContext.Database.BeginTransaction();
        }
    }
}
