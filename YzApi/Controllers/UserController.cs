﻿using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YzApi.Models.User;

namespace YzApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class UserController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public UserController(MysqlDbContext dbContext) : base(dbContext)
        {

        }

        /// <summary>
        /// 使用手机号码和短信验证完成注册，完成登录
        /// </summary>
        /// <remarks>
        /// 同时生成包含用户id和authCode的cookie, ，有效期365天
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>新生成用户id</returns>
        [HttpPost]
        public int Register([FromBody] RegisterModel model)
        {
            return 0;
        }
    }
}
