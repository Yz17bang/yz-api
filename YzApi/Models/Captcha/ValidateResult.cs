﻿namespace YzApi.Models.Captcha
{
    /// <summary>
    /// Captcha验证结果
    /// </summary>
    public class ValidateResult : JsonResult
    {
        /// <summary>
        /// 用户输入是否正确
        /// </summary>
        public bool Correct { get; set; }
    }
}
