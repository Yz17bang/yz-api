﻿using System.ComponentModel.DataAnnotations;

namespace YzApi.Models.User
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// 用户填写的手机号码
        /// </summary>
        [Required(ErrorMessage = "* 手机号码不能为空")]
        public string Number { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "* 短信验证码不能为空")]
        public string CheckCode { get; set; }
    }
}
