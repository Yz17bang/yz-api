﻿using Microsoft.EntityFrameworkCore;

namespace YzApi
{
    /// <summary>
    /// 
    /// </summary>
    public class MysqlDbContext: DbContext
    {
        private IConfiguration _configuration;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public MysqlDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = _configuration.GetConnectionString("yzApi");
            optionsBuilder
                .UseMySql(connectionString, ServerVersion.Parse("5.7.38-log"))
#if DEBUG
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
#endif
                ;
            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            base.OnModelCreating(modelBuilder);
        }
    }
}
