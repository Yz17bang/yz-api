﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace YzApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ArticleController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public ArticleController(DbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/[controller]/{pageIndex}")]
        public string Index(int pageIndex = 1)
        {
            return pageIndex.ToString();
        }

        /// <summary>
        /// 获取当个文章（Article）的内容
        /// </summary>
        /// <param name="id">文章Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/[controller]/{id}")]
        public string Single(int id)
        {
            return id.ToString();
        }
    }
}
