﻿namespace YzApi.Models
{
    /// <summary>
    /// 所有响应前端的Json，都应该继承这个类，
    /// 并且将“可供前端处理的”报错信息写入Error
    /// </summary>
    public class JsonResult
    {
        /// <summary>
        /// 可供前端处理的报错信息，比如：用户输入格式错误
        /// 注意：不要包含后端捕获（catch）的异常（Exception）信息，
        /// 程序运行的报错（Exception）由其他机制处理
        /// </summary>
        public string Error { get; set; }
    }
}
